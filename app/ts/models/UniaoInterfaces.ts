import { Imprimivel, Igualavel } from './index';

export interface UniaoInterfaces<T> extends Imprimivel, Igualavel<T>  { }